#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.SceneManagement;

namespace WainWork.SceneManagement
{
    public static partial class SceneConfigurator
    {
        [MenuItem("Tools/Scene configurator/Prepare current scene to runtime ^W")]
        public static void PrepareRuntimeCurrentScene()
        {
            var sceneInitializer = FindInitializer();
            if (sceneInitializer == default)
                return;

            sceneInitializer.PrepareRuntime();
            var scene = GetCurrentScene();
            EditorSceneManager.MarkSceneDirty(scene);
            EditorSceneManager.SaveScene(scene);
        }

        [MenuItem("Tools/Scene configurator/Prepare current scene to editing ^E")]
        public static void PrepareEditingCurrentScene()
        {
            var sceneInitializer = FindInitializer();
            if (sceneInitializer == default)
                return;

            sceneInitializer.PrepareEditing();
            var scene = GetCurrentScene();
            EditorSceneManager.MarkSceneDirty(scene);
        }
    }
}

#endif