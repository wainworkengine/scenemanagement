#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEngine.SceneManagement;

namespace WainWork.SceneManagement
{
    public static partial class SceneConfigurator
    {
        private static IEditorSceneInitializer FindInitializer()
        {
            var scene = GetCurrentScene();

            var sceneInitializers = new List<IEditorSceneInitializer>();

            foreach (var root in scene.GetRootGameObjects())
                sceneInitializers.AddRange(root.GetComponentsInChildren<IEditorSceneInitializer>());

            if (sceneInitializers.Count == 0)
            {
                EditorUtility.DisplayDialog(
                    "Error in scene",
                    $"Scene [{scene.name}] dont have any {nameof(ISceneInitializer)}",
                    "OK");
                return default;
            }

            if (sceneInitializers.Count > 1)
            {
                EditorUtility.DisplayDialog(
                    "Error in scene",
                    $"Scene [{scene.name}] have more than 1 {nameof(ISceneInitializer)}",
                    "OK");
                return default;
            }

            return sceneInitializers[0];
        }
        
        private static Scene GetCurrentScene() => SceneManager.GetActiveScene();
    }
}

#endif