#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

namespace WainWork.SceneManagement
{
    [CustomEditor(typeof(SceneInitializer), true)]
    public class SceneInitializerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GetEditorSceneInitializer(out var sceneInitializer))
                DrawButtons(sceneInitializer);

            base.OnInspectorGUI();
        }

        protected virtual void DrawButtons(IEditorSceneInitializer sceneInitializer)
        {
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Prepare"))
            {
                sceneInitializer.PrepareRuntime();
                var scene = UnitySceneManager.GetActiveScene();
                EditorSceneManager.MarkSceneDirty(scene);
                EditorSceneManager.SaveScene(scene);
            }

            if (GUILayout.Button("Edit"))
            {
                sceneInitializer.PrepareEditing();
            }

            GUILayout.EndHorizontal();
        }

        protected virtual bool GetEditorSceneInitializer(out IEditorSceneInitializer sceneInitializer)
        {
            sceneInitializer = default;

            if (serializedObject.targetObject is IEditorSceneInitializer sceneInitializer2)
                sceneInitializer = sceneInitializer2;
            else if (serializedObject.targetObject is GameObject gameObject)
                if (gameObject.TryGetComponent<IEditorSceneInitializer>(out var sceneInitializer3))
                    sceneInitializer = sceneInitializer3;

            return sceneInitializer != default;
        }
    }
}

#endif